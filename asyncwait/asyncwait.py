#!/usr/bin/env python3
import asyncio
import random


async def sleep():
    n = random.randint(1, 5)
    await asyncio.sleep(n)
    return n


async def exception_in_2s():
    await asyncio.sleep(2)
    raise Exception('Bang!')


async def main():
    fs = []
    for _ in range(10):
        fs.append(sleep())
    fs.append(exception_in_2s())
    while fs:
        done, fs = await asyncio.wait(fs, return_when='FIRST_COMPLETED')
        for d in done:
            try:
                print(d.result())
            except Exception as e:
                print(f'Exception: {e}')
        print('')

# Python 3.7
# asyncio.run(main())

# Python 3.6
loop = asyncio.get_event_loop()
loop.run_until_complete(main())
