#include <stdio.h>
#include <setjmp.h>

int main(void) {
	jmp_buf env;
	int val = 0;
	switch (val = setjmp(env)) {
		case 0:
			printf("%d\n", val);
			val++;
			longjmp(env, val);
		case 1:
			printf("%d\n", val);
			val++;
			longjmp(env, val);
		case 2:
			printf("%d\n", val);
			val++;
			longjmp(env, val);
		case 3:
			printf("%d\n", val);
	}
	return 0;
}
