/* make directory */

#include <stdio.h>
#include <stdlib.h>
#include <sys/stat.h>

char *parse_args(int argc, char **argv) {
	if (argc != 2) {
		fprintf(stderr, "Usage: %s <path>\n", argv[0]);
		exit(1);
	}
	return argv[1];
}

void make_directory(char *path) {
	__mode_t mask = umask(0);
	if (mkdir(path, 0777 - mask) != 0) {
		perror("mkdir");
		exit(1);
	}
}

int main(int argc, char **argv)
{
	char *path = parse_args(argc, argv);
	make_directory(path);
	return 0;
}
