#include <errno.h>
#include <spawn.h>
#include <stdio.h>
#include <string.h>
#include <sys/types.h>
#include <sys/wait.h>

int main() {
	pid_t child_pid;
	int rc, status;
	char *argv[] = {"sleep", "3", NULL};
	char *envp[] = {NULL};
	rc = posix_spawn(&child_pid, "/bin/sleep", NULL, NULL, argv, envp);
	if (rc != 0) {
		printf("posix_spawn: %s\n", strerror(rc));
		return 1;
	}
	rc = waitpid(child_pid, &status, 0);
	if (rc == -1) {
		perror("waitpid");
		return 1;
	}
	if (!WIFEXITED(status)) {
		printf("Child did not exit normally.\n");
		return 1;
	} else {
		printf("Child return value: %d\n", WEXITSTATUS(status));
	}
	return 0;
}
