#include <stdio.h>
#include <stdarg.h>

int DEBUG = 0;

void debug(const char *format, ...) {
    if (!DEBUG)
        return;
    va_list ap;
    va_start(ap, format);
    vprintf(format, ap);
}

int main(int argc, char **argv) {
    DEBUG = 1;
    debug("Hello %s%s%s", "World", "!", "\n");
    return 0;
}
