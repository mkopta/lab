#include <stdio.h>
#include <stdlib.h>
#include <string.h>

void usage(char *prgname) {
	printf("%s: <suffix> <text>\n", prgname);
}

int endswith(char *suffix, char *text) {
	size_t suffix_len = strlen(suffix);
	size_t text_len = strlen(text);
	if (suffix_len > text_len)
		return 0;
	if (strcmp(suffix, (text_len - suffix_len) + text) == 0)
		return 1;
	return 0;
}

int main(int argc, char **argv) {
	if (argc != 3) {
		usage(argv[0]);
		exit(1);
	}
	printf("%d\n", endswith2(argv[1], argv[2]));
	return 0;
}
