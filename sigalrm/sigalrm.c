#include <stdio.h>
#include <unistd.h>
#include <signal.h>

void handler(int sig) {
	printf("signal: %d\n", sig);
}

int main(void) {
	printf("begin\n");
	signal(SIGALRM, handler);
	alarm(3);
	printf("sleeping\n");
	sleep(4);
	printf("end\n");
	return 0;
}
