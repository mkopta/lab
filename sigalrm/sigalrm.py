import time
import signal

def handler(sig, _):
	print("signal: %s" % sig)

print("begin")
signal.signal(signal.SIGALRM, handler)
signal.alarm(3)
print("sleeping")
time.sleep(4)
print("end")
