import time
import signal

def handler(sig, _):
	print("signal: %s" % sig)
	raise Exception()

print("begin")
signal.signal(signal.SIGALRM, handler)
signal.alarm(3)
print("sleeping")
try:
	time.sleep(4)
except:
	print("exception thrown")
print("end")
