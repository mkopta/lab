#include <stdio.h>
#include <stdarg.h>

int DEBUG = 0;

/* AFAIK there is no way to pass-through the va_list
 * as-is to another function.
 * e.g. this isn't possible:
 *
 * void debug(const char *format, ...) {
 *     if (!DEBUG) return;
 *     printf(format, ...);
 * }
 *
 * For that reason, each variadic function should
 * have 'v' prefixed counterpart.
 * E.g. printf() and vprintf().
 * This should apply to custom variadic functions as well,
 * so here I am define not only the debug(), but also
 * vdebug(), which is the actual implementation, while
 * the debug() is just a variadic wrapper.
 */

void vdebug(const char *format, va_list ap) {
	if (!DEBUG)
		return;
	vprintf(format, ap);
}

void debug(const char *format, ...) {
	va_list ap;
	va_start(ap, format);
	vdebug(format, ap);
}

int main(int argc, char **argv) {
    DEBUG = 1;
    debug("Hello %s%s%s", "World", "!", "\n");
    return 0;
}
