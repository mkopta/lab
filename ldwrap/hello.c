#include <stdio.h>

int main() {
	/*
	Using printf("Hello World!\n");
	would allow the compiler to
	optimize to puts() even despite -O0.
	There, I need to force no optimization
	by sending an argument.
	*/
	printf("Hello %s!\n", "World");
	return 0;
}
