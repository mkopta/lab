#include <stdio.h>
#include <stdarg.h>

int __real_printf(const char *format, ...);

int __wrap_printf(const char *format, ...) {
	va_list ap;
	va_start(ap, format);
	puts("Inside the __wrap_printf.");
	__real_printf("Calling real printf\n");
	return vprintf(format, ap);
}
