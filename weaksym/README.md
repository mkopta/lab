# Weak symbol override with clang

https://www.reddit.com/r/C_Programming/comments/biaynw/weak_symbol_override_with_clang/

Hi everyone,

I am trying to understand weak symbols (for the  purpose of unit testing) and have this working example which defines  function `greeting()` twice with different implementations.

[https://gitlab.com/mkopta/lab/tree/master/weaksym](https://gitlab.com/mkopta/lab/tree/master/weaksym)

`czech.c`

    #include <stdio.h>
    
    void greeting() {
    	printf("Ahoj!\n");
    }

`weaksym.c`

    #include <stdio.h>
    
    void greeting() {
    	printf("Hello!\n");
    }
    
    int main() {
    	greeting();
    	return 0;
    }

`Makefile`

    CC=gcc
    CFLAGS=-Wall -Wextra -pedantic -std=c99
    
    all:
    	$(CC) $(CFLAGS) -c -o weaksym.o weaksym.c
    	$(CC) $(CFLAGS) -c -o czech.o czech.c
    	objcopy --weaken-symbol=greeting weaksym.o weaksym.o
    	$(CC) $(CFLAGS) -o weaksym weaksym.o czech.o
    
    clean:
    	rm -f *.o weaksym

When  compiling, I am using `objcopy` to weaken the `greeting()` symbol in  the `weaksym.o`. I use `objcopy` because I would like to be able to weaken the symbols  without changing the code. That way, I could mock almost any symbol without touching the original code.

Compile with `make`, run as  `./weaksym` and you should get output "`Ahoj!`". This works because GCC  (or more precisely, the `ld` linker I guess) will resolve the symbol  conflict by using the strong definition of `greeting()` from `czech.o`  over the weak definition.

Now, when compiled with clang as `make CC=clang`, the `./weaksym` now outputs "`Hello!`". There are no errors or  warnings, so despite the double definition, it seems clang understands that one of the definitions is weak and deals with it. However, it does  the opposite of what GCC does - it uses the weak symbol over the strong.

I read through parts of the clang documentation and few StackOverflow  discussions, but did not learn much.

Do you know whether weak symbols should be overridden when linking objects with clang? If so, what am I  doing wrong? Am I missing some special argument to clang?

Thanks for all comments!


## skeeto

I was having trouble figuring out what was going on until I enabled optimization. Check this out:
```
$ make CC=gcc CFLAGS=-O0
$ ./weaksym
Ahoj!
$ make CC=gcc CFLAGS=-O3
$ ./weaksym
Hello!
```
Behavior from GCC changes depending on optimization. When `-O3` is enabled, `greeting` is inlined into `main` and the decision is taken away from the linker. This is what always happens with Clang. Observe:
```
$ make CC=clang CFLAGS=-Os
$ objdump -d -Mintel weaksym.o
000000000000000a <main>:
   a:   50                      push   rax
   b:   bf 00 00 00 00          mov    edi,0x0
  10:   e8 00 00 00 00          call   15 <main+0xb>
  15:   31 c0                   xor    eax,eax
  17:   59                      pop    rcx
  18:   c3                      ret
```
Notice it's clearing `edi` with zero before the call? That's a very inefficient instruction for clearing `edi`, since it should be using `xor edi, edi`. And why clear it at all? Well, that's actually a relocation, to be patched in with the string address. This is a call to `printf` not `greeting`, which takes no arguments.

We can verify this with `readelf -r`:
```
$ readelf -r weaksym.o

Relocation section '.rela.text' at offset 0x200 contains 4 entries:
  Offset          Info           Type           Sym. Value    Sym. Name + Addend
000000000001  00030000000a R_X86_64_32       0000000000000000 .rodata.str1.1 + 0
000000000006  000a00000004 R_X86_64_PLT32    0000000000000000 puts - 4
00000000000c  00030000000a R_X86_64_32       0000000000000000 .rodata.str1.1 + 0
000000000011  000a00000004 R_X86_64_PLT32    0000000000000000 puts - 4

Relocation section '.rela.eh_frame' at offset 0x260 contains 2 entries:
  Offset          Info           Type           Sym. Value    Sym. Name + Addend
000000000020  000200000002 R_X86_64_PC32     0000000000000000 .text + 0
000000000034  000200000002 R_X86_64_PC32     0000000000000000 .text + a
```

The `mov edi, 0` instruction starts at offset `b`. That puts the relocation at `c` (just after the opcode). Note the relocation offset at `c` for `.rodata.str1.1` (e.g. a string). The function call at offset `10` has a relocation at offset `11` into the PLT: a dynamic function call. That's `printf`.

So your problem is that the weak symbol you're trying to replace is in the same translation unit as the caller. The linker isn't necessarily involved in this call, so you can't reliably override it. Move it out to another translation unit.

## oh5nxo

clang `-no-integrated-as` does something differently, instead of
```
  2b:   e8 d0 ff ff ff          call   0 <greeting>
```
it turns to
```
  2b:   e8 fc ff ff ff          call   2c <main+0xc>
                        2c: R_386_PC32  greeting
```
