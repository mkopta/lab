#include <stdio.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <unistd.h>

int main() {
    int wait, wstatus;
    pid_t pid = fork();
    if (pid > 0) {
        puts("parent: here");
        wait = waitpid(pid, &wstatus, 0);
        if (wait != pid) {
            perror("parent: waipid");
            return 1;
        }
        puts("parent: Child seems to have exited.");
        if (WIFEXITED(wstatus)) {
            puts("parent: Yep, child exited.");
            printf("parent: child status: %d\n", WEXITSTATUS(wstatus));
        } else {
            puts("parent: Hmm, it did not.");
        }
    } else if (pid == 0) {
        puts("child: here");
        return 2;
    } else {
        perror("fork");
        return 1;
    }
    return 0;
}
